# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.
  config.vm.provider :libvirt do |domain|
    domain.driver = "kvm"
    domain.host = 'localhost'
    domain.uri = 'qemu:///system'
    domain.default_prefix = "vagrant_"
    domain.cpus=2
    domain.memory=2048
  end

  config.vm.define :"centos8-docker"
  config.vm.box = "centos/8"
  config.vm.hostname = "centos8-docker"
  
  config.ssh.insert_key = 'true'
  config.ssh.keys_only = 'false'
  config.ssh.forward_agent = 'true'
  #config.ssh.username = 'root'
  #config.ssh.password = 'vagrant'

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  config.vm.network "forwarded_port", guest: 5000, host: 5000, host_ip: "0.0.0.0"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  #config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "./ansible", "/root/ansible", type: "rsync"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL

     # Install packages
     dnf -y install epel-release firewalld
     dnf -y install ansible

     # Install modulo docker to ansible
     ansible-galaxy collection install community.docker

     # Create a key to ansible works 
     ssh-keygen -t rsa -b 4096 -f /root/.ssh/id_rsa -q -N '' 
     cat /root/.ssh/id_rsa.pub > /root/.ssh/authorized_keys

     # Dot not check key localhost
     echo -e "Host localhost\n\tStrictHostKeyChecking no\n\tUserKnownHostsFile=/dev/null" > /root/.ssh/config

     # Allow password authentication
     sed -i 's/^PasswordAuthentication no$/PasswordAuthentication yes/g' /etc/ssh/sshd_config

     # Enable firewalld
     systemctl enable --now firewalld.service

     # Run playbook 
     cd /vagrant/ansible
     ansible-playbook -i hosts/localhost.yaml playbook.yaml

     # Add user vagrant to allow access via ssh
     groupmems -a vagrant -g infra

     # Restart ssh service to reload config
     systemctl restart sshd.service
   SHELL
end
