.PHONY: image publish run run-docker test

# if you want to use your own registry, change "REGISTRY" value
#REGISTRY       = your.url.registry
REGISTRY_USER   = robsonjorgefernandez
NAME            = players-app
IMAGE           = $(REGISTRY_USER)/$(NAME):$(VERSION)

image: guard-VERSION ## Build image
	docker build -t $(IMAGE) .

publish: guard-VERSION ## Publish image
	docker push $(IMAGE)

run: ## Run locally
	go env -w GO111MODULE=off
	go run ./src

run-docker: guard-VERSION ## Run docker container
	docker run --rm -d --name $(NAME) -d -p 5000:5000 $(REGISTRY_USER)/$(NAME):$(VERSION)

test:
	go env -w GO111MODULE=off
	go test -coverprofile=coverage.out ./src/...

guard-%:
	@ if [ "${${*}}" = ""  ]; then \
		echo "Variable '$*' not set"; \
		exit 1; \
	fi
